#!/bin/sh

# # CLI usage
# if [ $# -lt 1 ]
# then
#   echo 1>&2 "$0: not enough arguments\n"
#   echo "Usage: ckv.sh <check_id>\n"
#   exit 1
# fi

# # Variables settings
# ID=$1

# YAML Format
# CMD="checkov --framework cloudformation --external-checks-dir . -c case1 -f cfn.yaml --compact"

# JSON Format
# CMD="checkov --framework cloudformation --external-checks-dir . -c case1 -f cfn.json --compact"

# Whole Directory
CMD="checkov --framework cloudformation --external-checks-dir . -c case1 -d . --compact"

# Checkov Scan
echo "### Executing: $CMD"
$CMD
