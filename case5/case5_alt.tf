### Scenario 5
##  Ensure AWS IAM Role is limited to no more than 1 principal

/*
UPDATED 11.30 to include inline trust policy as well using a data.iam_policy_document
NOTE - this should illustrate the challenges of policies geared towards scanning configuration files vs plans
 Either need to check for inline policy or identify relationship and then check referenced data.
 This same "challenge" applies to permission-type policies as well, which can either be inline, a data reference, OR via an attachmed managed policy
 In that scenario, it may make more sense to have a policy that is oriented around principals, and another around managed policies.
*/

## Trust policy inserted via data
resource "aws_iam_role" "test_role_B_pass" {
  name               = "test_role_B_pass"
  assume_role_policy = data.aws_iam_policy_document.scenario_5_pass1.json

  tags = {
    tag-key = "tag-value"
  }
}
data "aws_iam_policy_document" "scenario_5_pass1" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["s3.amazonaws.com"]
    }
  }
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

## Trust policy inserted via data
resource "aws_iam_role" "test_role_B_fail" {
  name               = "test_role_B_fail"
  assume_role_policy = data.aws_iam_policy_document.scenario_5_fail1.json

  tags = {
    tag-key = "tag-value"
  }
}
data "aws_iam_policy_document" "scenario_5_pass2" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["s3.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "scenario_5_fail1" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com", "s3.amazonaws.com"]
    }
  }
}

## Trust policy inserted inline
resource "aws_iam_role" "test_role_A_pass" {
  name = "test_role_A_pass"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = ["sts:AssumeRole"]
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = ["ec2.amazonaws.com"]
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}

## Trust policy inserted inline
resource "aws_iam_role" "test_role_A_fail" {
  name = "test_role_A_fail"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = ["sts:AssumeRole"]
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = ["ec2.amazonaws.com", "s3.amazonaws.com"]
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}

## Trust policy inserted inline
resource "aws_iam_role" "test_role_C_pass" {
  name = "test_role_C_pass"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = ["sts:AssumeRole"]
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = ["123456789012"]
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}

## Trust policy inserted inline
resource "aws_iam_role" "test_role_C_fail" {
  name = "test_role_C_fail"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = ["sts:AssumeRole"]
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS : [
            "123456789012",
            "555555555555"
          ]
        }
      },
    ]
  })

  tags = {
    tag-key = "tag-value"
  }
}




# data "a" "pass001" {
#   statement {
#     principals {
#       identifiers = ["a"]
#     }
#     principals {
#       identifiers = ["b"]
#     }
#   }
# }
# data "a" "fail001" {
#   statement {
#     principals {
#       identifiers = ["a","b"]
#     }
#     principals {
#       identifiers = ["c"]
#     }
#   }
# }
# data "a" "fail002" {
#   statement {
#     principals {
#       identifiers = ["a","b","c"]
#     }
#     principals {
#       identifiers = ["d","e","f"]
#     }
#   }
# }
