### Scenario 1 
##  IAM Policy documents that have statements granting any s3 actions 
## that have a * in any of the specified resources for that statement.

resource "aws_iam_role_policy" "scenario_1_fail1" {
  name   = "scenario-1-fail1"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_fail1.json
}

data "aws_iam_policy_document" "scenario_1_fail1" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = ["*"] ### FAIL
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
}

resource "aws_iam_role_policy" "scenario_1_fail2" {
  name   = "scenario-1-fail2"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_fail2.json
}

data "aws_iam_policy_document" "scenario_1_fail2" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = ["arn:aws:s3:::cnc-*"] ### FAIL
  }
}

resource "aws_iam_role_policy" "scenario_1_fail3" {
  name   = "scenario-1-fail3"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_fail3.json
}

data "aws_iam_policy_document" "scenario_1_fail3" {
  statement {
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
  statement {
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = ["arn:aws:s3:::cnc-*"] ### FAIL
  }
}

#######################################################

resource "aws_iam_role_policy" "scenario_1_pass1" {
  name   = "scenario-1-pass1"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_pass1.json
}

data "aws_iam_policy_document" "scenario_1_pass1" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
}

resource "aws_iam_role_policy" "scenario_1_pass2" {
  name   = "scenario-1-pass2"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_pass2.json
}

data "aws_iam_policy_document" "scenario_1_pass2" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-bar-foo",
      "arn:aws:s3:::cnc-example-bucket-bar-foo/*" ### PASS
    ]
  }
}

resource "aws_iam_role_policy" "scenario_1_pass3" {
  name   = "scenario-1-pass3"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_pass3.json
}

data "aws_iam_policy_document" "scenario_1_pass3" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole", ### PASS
      "ec2:*"           ### PASS
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

#######################################################

resource "aws_iam_role" "example_role" {
  name               = "example_role"
  assume_role_policy = data.aws_iam_policy_document.scenario_1_pass1.json

  tags = {
    tag-key = "tag-value"
  }
}
