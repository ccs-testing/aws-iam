#!/bin/sh

# # CLI usage
# if [ $# -lt 1 ]
# then
#   echo 1>&2 "$0: not enough arguments\n"
#   echo "Usage: ckv.sh <check_id>\n"
#   exit 1
# fi

# # Variables settings
# ID=$1

# Checkov CLI command
# CMD="checkov --framework terraform --external-checks-dir . -c case1 -f case$ID.tf"
# CMD="checkov --framework terraform --external-checks-dir . -c case1 -f case1.tf --compact"
# CMD="checkov --framework terraform_plan --external-checks-dir . -c case1 -f tfplan.json --compact"
CMD="checkov --external-checks-dir . -c case1 -f case1.tf -d . --compact"

echo "### Executing: $CMD"

# Checkov scan
$CMD
